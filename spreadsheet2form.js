function crearForm() {
    var doc = SpreadsheetApp.getActiveSpreadsheet()
    var sheet = doc.getSheetByName("PREGUNTES");
  
    //Creem formulari amb el nom del spreadsheet, el fem evaluable d'única resposta...
    var form = FormApp.create(doc.getName() + " - Form");
    form.setIsQuiz(true)
        .setLimitOneResponsePerUser(true)
        .setShuffleQuestions(true)
        .setRequireLogin(true);

    var range = sheet.getRange(1, 1, 300, 7); // llegim 300 files, 7 columnes
    var values = range.getValues();

    for (var row in values) {
        if (values[row][0] == 'Question') {
            var preguntaTest = form.addMultipleChoiceItem();
            // la columna 1 té la pregunta
            preguntaTest.setTitle(values[row][1]);
            // les columnes de 2 a 5 son les 4 respostes possibles, 
            // la columna 6 té la resposta correcta
            preguntaTest.setChoices([
                preguntaTest.createChoice(values[row][2], values[row][2] == values[row][6]),
                preguntaTest.createChoice(values[row][3], values[row][3] == values[row][6]),
                preguntaTest.createChoice(values[row][4], values[row][4] == values[row][6]),
                preguntaTest.createChoice(values[row][5], values[row][5] == values[row][6]),
            ]);
            preguntaTest.showOtherOption(false).setPoints(1);
        
        } else if (values[row][0] == 'Break') {
            form.addPageBreakItem().setTitle(values[row][1]);

        } else if (values[row][0] == 'Password') {
            var preguntaEscriure = form.addTextItem();
            preguntaEscriure.setTitle(values[row][1]);
            preguntaEscriure.setRequired(true);
            // la columna 6 té la resposta correcta
            var textValidation = FormApp.createTextValidation().requireNumberEqualTo(values[row][6])
                .setHelpText('Codi incorrecte').build();
            preguntaEscriure.setValidation(textValidation);
        }
    }
}